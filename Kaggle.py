import Run
import Models

def main():
    model_list = ["fourth_gru"]
    batch_size = 256
    periods = 50
    epochs = 100
    Run.predict_kaggle(model_list, periods, epochs, batch_size)

if __name__ == "__main__":
    main()