import Run
import Models

def main():
    model_list = list(Models.prepared_models.keys())
    model_list += model_list
    model_list += model_list

    Run.run_for_models(model_list, 50, 300, 256)
    Run.run_for_models(model_list, 100, 300, 256)
    Run.run_for_models(model_list, 200, 300, 256)


if __name__ == "__main__":
    main()
