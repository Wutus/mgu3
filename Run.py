import Visualize
import DataSets
import Utils
import numpy as np
import pandas as pd
import keras
import tensorflow as tf
import os
import pickle
import random
import SoundsStats as SS
import Models

def prepare_dataframe(df, period):
    df["signal"] = df["audio_path"].map(
        SS.get_raw_signal)
    df["mfcc"] = df["signal"].map(
        lambda x: SS.get_mfcc(x, period))
    df["fbank"] = df["signal"].map(
        lambda x: SS.get_fbank(x, period))
    df["spectrogram"] = df["signal"].map(
        lambda x: np.transpose(np.log10(SS.get_spectrogram(x, period)[2])))

def process_dataframe_full_X(df):
    X = np.dstack((
        np.stack(df["mfcc"].values),
        np.stack(df["fbank"].values),
        np.stack(df["spectrogram"].values)))
    return X

def process_dataframe_full(df):
    X = np.dstack((
        np.stack(df["mfcc"].values),
        np.stack(df["fbank"].values),
        np.stack(df["spectrogram"].values)))
    Y = keras.utils.to_categorical(
        df["class"].map(lambda x: DataSets.get_class_int(x)))
    return (X, Y)

def process_dataframe_X(df):
    X = np.stack(df["mfcc"].values)
    return X

def process_dataframe(df):
    X = np.stack(df["mfcc"].values)
    Y = keras.utils.to_categorical(
        df["class"].map(lambda x: DataSets.get_class_int(x)))
    return (X, Y)


def test_model(model, X_train, Y_train, X_test, Y_test, epochs, batch_size):
    history = model.fit(
            X_train, Y_train,
            batch_size=batch_size, epochs=epochs, verbose=1,
            validation_data=(X_test, Y_test))
    model.summary()
    evaluate = model.evaluate(X_test, Y_test, batch_size=batch_size)
    (_, accuracy) = evaluate 
    print("Evaluate:", evaluate)
    return (history, accuracy)

def run(model, model_name, X_train, Y_train, X_test, Y_test, epochs, batch_size, seed, periods):
    print(f"Processing {model_name}")
    np.random.seed(seed)
    tf.random.set_seed(seed)
    date_str = Utils.get_date_str()
    dirname = f"{model_name}_{epochs}_{seed}_{periods}_{date_str}"
    fullpath = os.path.join("results", dirname)
    os.mkdir(fullpath)
    (history, accuracy) = test_model(
        model, X_train, Y_train, X_test, Y_test,
        epochs, batch_size)
    with open(os.path.join(fullpath, "history"), "wb") as f:
        pickle.dump(history.history, f)
    with open(os.path.join(fullpath, "accuracy"), "w") as f:
        f.write(f"{accuracy}")
    fs = open(f"{model_name}_{periods}_summary.txt", 'w')
    def print_summary_to_file(x):
        fs.write(x)
    model.summary(print_fn = print_summary_to_file)
    fs.close()
    model.save(os.path.join(fullpath, "model"))
    Y_pred_raw = model.predict(X_test, batch_size=batch_size)
    Y_pred_uncat = np.argmax(Y_pred_raw, axis=-1)
    Y_test_uncat = np.argmax(Y_test, axis=-1)
    Visualize.save_plots(Y_pred_uncat, Y_test_uncat, history, fullpath)

def run_for_models(model_list, periods, epochs, batch_size):
    df_train = DataSets.get_train2_dataset()
    df_test = DataSets.get_validation2_dataset()

    prepare_dataframe(df_train, periods)
    prepare_dataframe(df_test, periods)

    (X_train, Y_train) = process_dataframe(df_train)
    (X_test, Y_test) = process_dataframe(df_test)
    input_size = (X_train.shape[1], X_train.shape[2])
    output_shape = Y_train.shape[1]

    if (X_test.shape[1], X_test.shape[2]) != input_size:
        raise RuntimeError(f"X_train.shape[2] ({X_train.shape[2]}) different than X_test.shape[2] ({X_test.shape[2]})")

    if Y_test.shape[1] != output_shape:
        raise RuntimeError(f"X_train.shape[2] ({Y_train.shape[0]}) different than X_test.shape[2] ({Y_test.shape[0]})")

    for model_name in model_list:
        try:
            print(f"\nPreparing model {model_name}")
            model = Models.prepared_models[model_name](input_size, output_shape)
            run(model, model_name,
                X_train, Y_train, X_test, Y_test,
                epochs, batch_size, random.randint(1, 1000000), periods)
            print(f"Finished processing model {model_name}\n\n")
        except Exception as e:
            print(f"ERROR: Failed to process {model_name} - {e}")

def run_pred(model, model_name, X_train, Y_train, X_test, epochs, batch_size, seed, periods, df_test):
    print(f"Processing {model_name}")
    np.random.seed(seed)
    tf.random.set_seed(seed)
    date_str = Utils.get_date_str()
    dirname = f"pred_{model_name}_{epochs}_{seed}_{periods}_{date_str}"
    fullpath = os.path.join("results", dirname)
    os.mkdir(fullpath)
    model.fit(X_train, Y_train,
            batch_size=batch_size, epochs=epochs, verbose=1)
    model.save(os.path.join(fullpath, "model"))
    print("Model saved")
    Y_pred_raw = model.predict(X_test, batch_size=batch_size)
    print("Values predicted")
    Y_pred_uncat = np.argmax(Y_pred_raw, axis=-1)
    print("Values classified")
    Y_pred_labels = pd.Series(Y_pred_uncat).map(DataSets.get_class_name)
    print("Labels obtained")
    a = df_test["audio_filename"].map(lambda x: x.split("\\")[-1])
    result_df = pd.DataFrame({"fname": a, "label": Y_pred_labels})
    print("Results calculated")
    result_df.to_csv(
        os.path.join(fullpath, "submission.csv"),
        encoding='utf-8',
        index=False)


def predict_kaggle(model_list, periods, epochs, batch_size):
    # df_train = DataSets.get_train_full_dataset()
    # df_test = DataSets.get_test_full_dataset()
    df_train = DataSets.get_train3_dataset()
    df_test = DataSets.get_test3_dataset()
    print("Loaded data!")
    prepare_dataframe(df_train, periods)
    print("Processed train")
    prepare_dataframe(df_test, periods)
    print("Processed test")

    (X_train, Y_train) = process_dataframe(df_train)
    print("Processed train")
    X_test = process_dataframe_X(df_test)
    print("Processed test")
    input_size = (X_train.shape[1], X_train.shape[2])
    output_shape = Y_train.shape[1]

    for model_name in model_list:
        try:
            print(f"\nPreparing model {model_name}")
            model = Models.prepared_models[model_name](input_size, output_shape)
            run_pred(model, model_name,
                X_train, Y_train, X_test,
                epochs, batch_size, random.randint(1, 1000000), periods, df_test)
            print(f"Finished processing model {model_name}\n\n")
        except Exception as e:
            print(f"ERROR: Failed to process {model_name} - {e}")