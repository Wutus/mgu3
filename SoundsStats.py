from python_speech_features import mfcc
from python_speech_features import logfbank
import scipy.io.wavfile as wavfile
import matplotlib.pyplot as plt
from matplotlib import cm
import scipy.signal
import numpy as np


def get_raw_signal(filename):
    (rate, sig) =  wavfile.read(filename)
    sig = sig.copy()
    sig.resize((16000,), refcheck=False)
    return (rate, sig)


def get_mfcc(signal, periods):
    (rate, sig) = signal
    time = sig.shape[0] / rate
    winlen = 0.025
    winstep = (time - winlen)/periods
    mfcc_feat = mfcc(sig, rate, winlen=winlen, winstep=winstep)
    mfcc_feat = mfcc_feat[:periods, :]
    return mfcc_feat


def get_fbank(signal, periods):
    (rate, sig) = signal
    time = sig.shape[0] / rate
    winlen = 0.025
    winstep = (time - winlen)/periods
    fbank_feat = logfbank(sig, rate, winlen=winlen, winstep=winstep)
    fbank_feat = fbank_feat[:periods, :]
    return fbank_feat


def get_spectrogram(signal, periods):
    (rate, sig) = signal
    if rate != 16000:
        print("DAFUQ")
    noverlap = 32
    nfft = 1024
    nperseg = int((sig.shape[0] - noverlap) / periods) + noverlap
    frequencies, times, spectrogram = scipy.signal.spectrogram(
        sig, fs=rate, nperseg=nperseg, noverlap=noverlap, nfft=nfft)
    times = times[:periods]
    spectrogram = spectrogram[:, :periods]
    return frequencies, times, spectrogram


def draw_mfcc_plot(mfcc_feat):
    fig, ax = plt.subplots()
    mfcc_data = np.swapaxes(mfcc_feat, 0, 1)
    cax = ax.imshow(
        mfcc_data,
        interpolation='nearest',
        cmap=cm.coolwarm,
        origin='lower')
    ax.set_title('MFCC')
    plt.show()


def draw_fbank_plot(fbank_feat):
    fig, ax = plt.subplots()
    fbank_data = np.swapaxes(fbank_feat, 0, 1)
    cax = ax.imshow(
        fbank_data,
        interpolation='nearest',
        cmap=cm.coolwarm,
        origin='lower')
    ax.set_title('FBANK')
    plt.show()


def draw_spectrogram(frequencies, times, spectrogram):
    plt.pcolormesh(times, frequencies, np.log10(spectrogram))
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.show()