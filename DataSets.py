import pandas as pd
import os
import re


def reverse_list_to_dict(list):
    return {v: k for (k, v) in enumerate(list)}


main_word_list = [
    "yes", "no", 
    "up", "down", "left", "right",
    "on", "off", "stop", "go", 
    "silence", "unknown"
]

auxiliary_word_list = [
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    "bed", "bird", "cat", "dog", "happy", "house", "marvin", "sheila", "tree", "wow"
]

word_list = main_word_list + auxiliary_word_list

word_list_rev = reverse_list_to_dict(word_list)

def get_class_name(class_int):
    return word_list[class_int]

def get_raw_class_int(class_name):
    return word_list_rev[class_name]


def get_class_int(class_name):
    class_int = get_raw_class_int(class_name)
    if class_int >= len(main_word_list):
        class_int = word_list_rev["unknown"]
    return class_int


def get_dataset_filelist(path_to_audio_dir, path_to_txt, verbose=False):
    filenames = pd.read_csv(path_to_txt, names=["filename"], header=None)
    filenames = filenames["filename"]
    df = pd.DataFrame({
        "audio_filename": filenames, 
        "audio_path": [os.path.join(path_to_audio_dir, filename.replace("\\", "/")) for filename in filenames],
        "class": [re.split(r'\\|\/', filename)[0] for filename in filenames]})
    return df


def get_dataset_fulldir(path_to_audio_dir, path_to_txt, verbose=True):
    file_browsed = 0
    df = pd.DataFrame({"audio_filename": [], "audio_path": [], "class": []})
    for dir in os.listdir(path_to_audio_dir):
        path = os.path.join(path_to_audio_dir, dir)
        for filename in os.listdir(path):
            full_path = os.path.join(path, filename)
            file_browsed += 1
            if verbose and file_browsed % 5000 == 0:
                print(f"Browsed {file_browsed} files")
            df.append({
                "audio_filename": os.path.join(dir, filename),
                "audio_path":  full_path,
                "class": dir,
            }, ignore_index=True)
    return df


def get_dataset(path_to_audio_dir, path_to_txt, verbose=False):
    verbose = True
    if path_to_txt is None:
        return get_dataset_fulldir(path_to_audio_dir, verbose)
    else:
        return get_dataset_filelist(path_to_audio_dir, path_to_txt, verbose=False)


def get_train_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/testing_list.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_validation_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/validation_list.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_train_full_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = None
    return get_dataset(audio_dir_path, txt_path)


def get_test_dataset(path_to_audio_dir, verbose=True):
    file_browsed = 0
    df = pd.DataFrame({"audio_filename": [], "audio_path": []})
    for filename in os.listdir(path_to_audio_dir):
        full_path = os.path.join(path_to_audio_dir, filename)
        file_browsed += 1
        if verbose and file_browsed % 5000 == 0:
            print(f"Browsed {file_browsed} files")
        df.append({
            "audio_filename": filename,
            "audio_path":  full_path
        }, ignore_index=True)
    return df


def get_test_full_dataset():
    audio_dir_path = "data/test/audio"
    return get_test_dataset(audio_dir_path)


def get_train2_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/testing2.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_validation2_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/validation2.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_test2_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/testing2.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_train3_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/training_full.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_validation3_dataset():
    audio_dir_path = "data/train/audio"
    txt_path = "data/train/validation3.txt"
    return get_dataset(audio_dir_path, txt_path)


def get_test3_dataset():
    audio_dir_path = "data/test"
    txt_path = "data/train/testing_full.txt"
    return get_dataset(audio_dir_path, txt_path)
