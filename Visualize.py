import matplotlib.pyplot as plt
import numpy as np
import DataSets
from sklearn.metrics import confusion_matrix
import itertools
import os


def display_results(model, batch_size, history, x_test, y_test):
    _, acc = model.evaluate(x_test, y_test, batch_size=batch_size)
    print("Accuracy = %.2f " % (acc * 100.0) + "%")
    prediction = np.argmax(model.predict(x_test, batch_size=batch_size), axis=-1)

    # Show plots
    show_confusion_matrix(prediction, np.argmax(y_test, axis=1))
    show_plots(history)


def show_confusion_matrix(y_pred, y_real):
    target_names = DataSets.main_word_list
    cm = confusion_matrix(y_pred, y_real)
    cmap = plt.get_cmap('Blues')
    plt.figure(figsize=(20, 16))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title("Macierz pomyłek")
    plt.colorbar()
    # set labels
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, rotation=45)
    plt.yticks(tick_marks, target_names)

    thresh = cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, "{:,}".format(cm[i, j]),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('Poprawna klasa')
    plt.xlabel('Przewidziana klasa')
    plt.show()


def show_plots(history):
    # plot loss
    plt.subplot(211)
    # Cross Entropy Loss
    plt.title('Wartość entropii krzyżowej')
    plt.plot(history.history['loss'], color='blue', label='Zbiór treningowy')
    plt.plot(history.history['val_loss'], color='orange', label='Zbiór walidacyjny')
    plt.legend()
    # plot accuracy
    plt.subplot(212)
    plt.title('Dokładność klasyfikacji')
    plt.plot(history.history['accuracy'], color='blue', label='Zbiór treningowy')
    plt.plot(history.history['val_accuracy'], color='orange', label='Zbiór walidacyjny')
    plt.legend()
    plt.show()


def save_confusion_matrix(y_pred, y_real, dirname):
    target_names = DataSets.main_word_list
    plot_name = "confusion_matrix"
    extension = "png"
    filename = f"{plot_name}.{extension}"
    cm = confusion_matrix(y_pred, y_real)
    cmap = plt.get_cmap('Blues')
    plt.figure(figsize=(20, 16))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title("Macierz pomyłek")
    plt.colorbar()
    # set labels
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, rotation=45)
    plt.yticks(tick_marks, target_names)

    thresh = cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, "{:,}".format(cm[i, j]),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('Poprawna klasa')
    plt.xlabel('Przewidziana klasa')
    plt.savefig(os.path.join(dirname, filename))
    plt.clf()


def save_accuracy_plots(history, dirname):
    plot_name = "accuracy_plots"
    extension = "png"
    filename = f"{plot_name}.{extension}"
    # plot loss
    plt.subplot(211)
    # Cross Entropy Loss
    plt.title('Wartość entropii krzyżowej')
    plt.plot(history.history['loss'], color='blue', label='Zbiór treningowy')
    plt.plot(history.history['val_loss'], color='orange', label='Zbiór walidacyjny')
    plt.legend()
    # plot accuracy
    plt.subplot(212)
    plt.title('Dokładność klasyfikacji')
    plt.plot(history.history['accuracy'], color='blue', label='Zbiór treningowy')
    plt.plot(history.history['val_accuracy'], color='orange', label='Zbiór walidacyjny')
    plt.legend()
    plt.savefig(os.path.join(dirname, filename))
    plt.clf()


def save_plots(y_pred, y_real, history, dirname):
    save_confusion_matrix(y_pred, y_real, dirname)
    save_accuracy_plots(history, dirname)
