import pandas as pd
import numpy as np
import keras
import keras.utils
from datetime import datetime
import Visualize

def get_date_str():
    now = datetime.now()
    dt_string = now.strftime("%d%m%Y%H%M%S")
    return dt_string
