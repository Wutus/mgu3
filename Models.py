import keras
import tensorflow as tf
from tensorflow.keras.layers import *
physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], enable=True)


def get_basic_lstm(input_size, output_size):
    model = tf.keras.Sequential()
    model.add(LSTM(output_size, return_sequences=False, input_shape=input_size))
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_second_lstm(input_size, output_size):
    model = tf.keras.models.Sequential([
        LSTM(256, return_sequences=False, input_shape=input_size),
        Dense(output_size)
    ])
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_third_lstm(input_size, output_size):
    model = tf.keras.models.Sequential([
        LSTM(256, return_sequences=False, input_shape=input_size),
        Dense(256),
        Dense(128),
        Dense(64),
        Dense(32),
        Dense(output_size)
    ])
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_fourth_lstm(input_size, output_size):
    model = tf.keras.models.Sequential([
        LSTM(256, return_sequences=True, input_shape=input_size),
        LSTM(256, return_sequences=False, input_shape=input_size),
        Dense(256),
        Dense(128),
        Dense(64),
        Dense(32),
        Dense(output_size)
    ])
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_basic_gru(input_size, output_size):
    model = tf.keras.Sequential()
    model.add(GRU(output_size, return_sequences=False, input_shape=input_size))
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_second_gru(input_size, output_size):
    model = tf.keras.models.Sequential([
        GRU(256, return_sequences=False, input_shape=input_size),
        Dense(output_size)
    ])
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_third_gru(input_size, output_size):
    model = tf.keras.models.Sequential([
        GRU(256, return_sequences=False, input_shape=input_size),
        Dense(256),
        Dense(128),
        Dense(64),
        Dense(32),
        Dense(output_size)
    ])
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_fourth_gru(input_size, output_size):
    model = tf.keras.models.Sequential([
        GRU(256, return_sequences=True, input_shape=input_size),
        GRU(256, return_sequences=False, input_shape=input_size),
        Dense(256),
        Dense(128),
        Dense(64),
        Dense(32),
        Dense(output_size)
    ])
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_attention_network(input_size, output_size):
    # https://towardsdatascience.com/recognizing-speech-commands-using-recurrent-neural-networks-with-attention-c2b2ba17c837
    inputs = Input(shape=input_size)
    outputs = Bidirectional(
        LSTM(64, return_sequences=True), input_shape=input_size)(inputs)
    outputs = Bidirectional(LSTM(64, return_sequences=True))(outputs)
    outputs = Bidirectional(LSTM(64, return_sequences=False))(outputs)
    attention = Dense(128)(outputs)
    attention = Attention()([attention, outputs])
    attention = Activation('softmax')(outputs)
    outputs = Dot(axes=1)([attention, outputs])
    outputs = Dense(64)(outputs)
    outputs = Dense(32)(outputs)
    outputs = Dense(output_size)(outputs)
    model = tf.keras.Model(
        inputs=[inputs], outputs=outputs)
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model


def get_attention_dropout_network(input_size, output_size):
    # https://towardsdatascience.com/recognizing-speech-commands-using-recurrent-neural-networks-with-attention-c2b2ba17c837
    inputs = Input(shape=input_size)
    outputs = Bidirectional(
        LSTM(64, return_sequences=True), input_shape=input_size)(inputs)
    outputs = Dropout(0.5)(outputs)
    outputs = Bidirectional(LSTM(64, return_sequences=True))(outputs)
    outputs = Dropout(0.5)(outputs)
    outputs = Bidirectional(LSTM(64, return_sequences=False))(outputs)
    attention = Dense(128)(outputs)
    attention = Attention()([attention, outputs])
    attention = Activation('softmax')(outputs)
    outputs = Dot(axes=1)([attention, outputs])
    outputs = Dense(64)(outputs)
    outputs = Dense(32)(outputs)
    outputs = Dense(output_size)(outputs)
    model = tf.keras.Model(
        inputs=[inputs], outputs=outputs)
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
            optimizer='adam',
            metrics=['accuracy'])
    return model

prepared_models = {
    "basic_lstm": get_basic_lstm,
    "second_lstm": get_second_lstm,
    "third_lstm": get_third_lstm,
    "fourth_lstm": get_fourth_lstm,
    "basic_gru": get_basic_gru,
    "second_gru": get_second_gru,
    "third_gru": get_third_gru,
    "fourth_gru": get_fourth_gru,
    "attention_lstm": get_attention_network,
    "attention_dropout2": get_attention_dropout_network
}
